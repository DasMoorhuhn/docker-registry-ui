# Docker Registry UI

![](.readme-content/example.png)

## Getting started

```bash
docker-compose up --build
```
`http://localhost/html/index.html`

## CORS issues

Install a 'unblock CORS' plugin into the browser
