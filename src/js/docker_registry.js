const registryUrl = 'http://docker.lan:5000/v2/'


export function fetchCatalog()
{
  return fetch(registryUrl + '_catalog')
}


function fetchImage(imageName, tag)
{
  return fetch(registryUrl + `${imageName}/manifests/${tag}`)
}


function fetchTags(imageName)
{
  return fetch(registryUrl + `${imageName}/tags/list`)
}


function proceedImage(image)
{
  image.then(response => response.json())
    .then(response => response.json())
    .then(tags =>
    {
      tags['tags'].forEach(tag =>
      {
        let image = fetchImage(imageName, tag)
        image.then(response => response.json())
          .then(image =>
          {
            console.log(image)
            console.log(tag)
          })
      })
    })
}
