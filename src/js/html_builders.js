import {fetchCatalog} from "./docker_registry.js";

function buildImages()
{
  let catalog = fetchCatalog()
  console.log(catalog)

  catalog.then(response => response.json())
    .then(images =>
    {
      const imageContainer = document.getElementById('images-root');
      images['repositories'].forEach(image =>
      {
        const imageDiv = document.createElement('div');
        imageDiv.classList.add('image');

        const button = document.createElement('button')
        button.textContent = image
        button.classList.add('button')

        imageDiv.appendChild(button)

        imageContainer.appendChild(imageDiv)
      });
    })
}

buildImages()